class Config:
	LOG_NAME 			= 'Alcatraz Policy Engine' # Base name for logging
	EXECUTE				= True # Apply permissions to systems automatically
	REVIEW_FILE 		= 'pendingPermissions.txt' # Where to log actions for review before execution
	METADATA_URI 		= "dbname='policy_engine'  user='postgres' password='Password@123' host='postgres'" # URI to access the policy metadata
	LOCAL_METADATA_URI 	= "dbname='policy_engine'  user='postgres' password='Password@123' host='localhost'"
	HDFS_URI			= "http://eypoc-mn0.pocinet.local:50070/webhdfs/v1"
	HDFS_CON_STR		= "hdfs://eypoc-mn0.pocinet.local"
	HDFS_NN_PRINCIPAL	= "hdfs/eypoc-mn0.pocinet.local@POCINET.LOCAL"
	CLASSPATH			= "/policy/JARs/PolicyEnforcer.jar"
	METADATA			= "/Metadata/"