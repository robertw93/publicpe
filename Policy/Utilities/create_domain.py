import sys, psycopg2, config
from psycopg2.extensions import AsIs
from pyhive import hive
from sh import kinit

config = config.Config()
kinit("hadmin@POCINET.LOCAL", "-k", "-t", "./Utilities/hadmin.keytab")
peConn = psycopg2.connect(config.LOCAL_METADATA_URI)
peConn.autocommit = True
policy_cursor = peConn.cursor()

connection_hive = hive.Connection(**config.HIVE_URI)
hive_cursor = connection_hive.cursor()

buAbreviations = {
	'resources': 'rsc',
	'projects': 'prj'
}
domainAbreviations = {
	'assets': 'ast',
	'alcatraz': 'altz',
	'pegasus': 'pgs',
	'staff': 'staff'
}

def createRole(rolename, confidentiality):
	rolename = rolename + confidentiality
	if hive:
		hive_cursor.execute("CREATE ROLE `{}`".format(rolename))
		hive_cursor.execute("GRANT ROLE `{}` TO GROUP `{}`".format(rolename, rolename))
	labels = '|'.join(getLabelList(sys.argv[1], sys.argv[2], confidentiality))
	policy_cursor.execute("""
		INSERT INTO GROUP_LABEL_MAPPING (Name, Label_List)
		VALUES ('%s', '%s') RETURNING Id
	""", (AsIs(rolename), AsIs(labels)))
	group = policy_cursor.fetchone()
	policy_cursor.execute("INSERT INTO ROLE_GROUP_MAPPING (name, group_list) VALUES ('%s', '%s')", (AsIs(rolename), AsIs(group[0])))
	print rolename, 'created'

def getTypedLabel(labeltype, value):
	policy_cursor.execute("""
		SELECT id
		FROM Labels
		WHERE type = '%s' AND value = '%s'
	""", (AsIs(labeltype), AsIs(value)))
	label = policy_cursor.fetchone()
	if label:
		return str(label[0])
	return '0'

def getLabelList(bu, domain, confidentiality):
	label_list = []
	label_list.append(getTypedLabel("Grain", bu + "_" + domain))
	label_list.append(getTypedLabel("Classification", confidentiality))
	return label_list

try:
	bu = buAbreviations[sys.argv[1]]
except:
	bu = 'dft'
	pass
try:
	domain = domainAbreviations[sys.argv[2]]
except:
	domain = 'dft'
	pass
try:
	hive = sys.argv[3]
except:
	hive = False
grain = sys.argv[1] + "_" + sys.argv[2]
policy_cursor.execute("""
	INSERT INTO Labels (Type, Value)
	VALUES('%s', '%s')
""", (AsIs('Grain'), AsIs(grain)))

createRole(bu + '_' + domain + '_', 'gu')
createRole(bu + '_' + domain + '_', 'pii')