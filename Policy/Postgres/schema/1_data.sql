COPY LABELS(type,value) FROM '/docker-entrypoint-initdb.d/labels.csv' WITH (FORMAT csv);
COPY GROUP_LABEL_MAPPING(name,label_list) FROM '/docker-entrypoint-initdb.d/groups.csv' WITH (FORMAT csv);
COPY ROLE_GROUP_MAPPING(name,group_list) FROM '/docker-entrypoint-initdb.d/roles.csv' WITH (FORMAT csv);