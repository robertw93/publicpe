import sh, requests, json, psycopg2, syslog
from jpype import isJVMStarted, startJVM,getDefaultJVMPath, JClass
from psycopg2.extensions import AsIs
from requests_gssapi import HTTPSPNEGOAuth, OPTIONAL

"""
    This is the class that interacts with the HDFS file system.
    Anything that needs to access HDFS, such as permission checking or updating will go through here
"""
class HDFSClient:
    def __init__(self, config):
        """Initialise the Class, including setting auth and URI"""
        # General Config
        self.execute = config.EXECUTE
        syslog.openlog(config.LOG_NAME + "(HDFS)")

        # HDFS WebAPI definition for testing
        self.gssapi_auth = HTTPSPNEGOAuth(mutual_authentication=OPTIONAL)
        self.execute = config.EXECUTE
        self.baseURI = config.HDFS_URI

        # Load JARs
        if not isJVMStarted():
            startJVM(getDefaultJVMPath(), "-ea", "-Djava.class.path=%s" % config.CLASSPATH)
        self.hdfs = JClass("Enforcers.HDFSEnforcer")()


    def listDir(self, absDir):
        """Recursively lists directories and files, adds an attribute to the record and returns a list"""
        result = []
        r = requests.get("{}/{}?op=LISTSTATUS".format(self.baseURI, absDir[1:]), auth=self.gssapi_auth)
        if str(r.status_code) == "200":
            j = json.loads(r.text)
            for obj in j["FileStatuses"]["FileStatus"]:
                #add attribute and append to list of objects
                obj["absolutePath"] = "{}/{}".format(absDir,obj["pathSuffix"])
                result.append(obj)
        return result


    def setOwnership(self, path, user, group, permissions):
        print "Recursively updating {} to be owned by {}:{} with POSIX permissions of {}".format(path, user, group, permissions)
        if self.execute:
            hdfs.setFileOwnership(path, user, group, permissions)


    def setACLUser(self, path, user, permissions):
        print "Recursively setting ACL for {}, allowing {} to have {} permissions".format(path, user, permissions)
        if self.execute:
            hdfs.setFileACLUser(path, user, permissions)


    def setACLGroup(self, path, group, permissions):
        print "Recursively setting ACL for {}, allowing {} to have {} permissions".format(path, group, permissions)
        if self.execute:
            hdfs.setFileACLGroup(path, group, permissions)

    def revokeACL(self, path, type, member):
        print "Recursively removing ACL from {}, revoking access from {}".format(path, member)
        if self.execute:
            hdfs.setFileACLUser(path, user, permissions)