import sys, os, json, syslog, HiveController, PolicyController, HDFSController, config, jpype, datetime
from sh import kinit

print datetime.datetime.now()
system_config = config.Config()
# Renew our kerberos ticket with hadmin
kinit("hadmin@POCINET.LOCAL", "-k", "-t", "/hadmin.keytab")
HDFSClient = HDFSController.HDFSClient(system_config)
HiveClient = HiveController.HiveClient(system_config)
PolicyClient = PolicyController.PolicyClient(system_config)
syslog.openlog(system_config.LOG_NAME)


def init():
    # Remove previous review files
    try:
        open(system_config.REVIEW_FILE, 'w').close()
    except:
        pass
    with open(system_config.METADATA + "hiveClassifications.json") as hive_json:
        hive = json.load(hive_json)
    return (hive)


if __name__ == '__main__':
    hiveClassification = init()
    currentState = {}
    print '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Starting permission and privilege check ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
    print 'SYSTEM: Hive'
    databases = HiveClient.executeQuery("SHOW DATABASES")
    while databases.next():
        db = databases.getString("database_name")
        tables = HiveClient.executeQuery("SHOW TABLES IN {}".format(db))
        while tables.next():
            tbl = str(tables.getString("tab_name"))
            tableString = "{}.{}".format(db, tbl)
            if tableString in hiveClassification:
                currentClassification = hiveClassification[tableString]
            else:
                currentClassification = { "all": [], "select": ['dft_dft_gu'] } # TODO: put default service groups here
            for permission in currentClassification:
                HiveClient.revokePermissions(currentClassification[permission], permission, db, tbl)
                for role in currentClassification[permission]:
                    HiveClient.grantPermissions(role, permission, db, tbl)
            currentState[tableString] = currentClassification
    with open(system_config.METADATA + "hiveCurrentState.json", "w+") as hive_current_state:
        hive_current_state.write(json.dumps(currentState, indent=4, sort_keys=True, separators=(',', ': '), ensure_ascii=False))
    
    
    if jpype.isJVMStarted():
        print datetime.datetime.now()
        jpype.shutdownJVM()
        print datetime.datetime.now()
